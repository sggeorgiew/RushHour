﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RushHour.Data.Entities;
using RushHour.Data.Repositories;

namespace RushHour.Services
{
    public class AppointmentService : BaseService<Appointment, AppointmentRepository, UnitOfWork>
    {
        protected override UnitOfWork UnitOfWork { get; set; }
        protected override AppointmentRepository Repository { get; set; }

        public AppointmentService(IValidationDictionary validationDictionary) : base(validationDictionary)
        {
            UnitOfWork = new UnitOfWork();
            Repository = UnitOfWork.AppointmentRepository;
        }

        protected override bool PreValidate(Appointment appointmentToValidate)
        {
            if (appointmentToValidate.StartDateTime == DateTime.MinValue)
            {
                this.validationDictionary.AddError("StartDateTime", "Start Date is required");
            }

            return this.validationDictionary.IsValid;
        }

        public bool HaveAccess(Appointment appointment, int loggedUserId)
        {
            return appointment == null || appointment.UserId == loggedUserId;
        }

        public IQueryable<Activity> GetAllActivities()
        {
            return UnitOfWork.ActivityRepository.GetAll();
        }

        public Activity GetActivityById(int id)
        {
            return UnitOfWork.ActivityRepository.GetById(id);
        }
    }
}
