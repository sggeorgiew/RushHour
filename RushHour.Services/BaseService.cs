﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using RushHour.Data.Entities;
using RushHour.Data.Repositories;

namespace RushHour.Services
{
    public abstract class BaseService<TEntity, TRepository, TUnitOfWork> 
        where TEntity : BaseEntity 
        where TRepository : BaseRepository<TEntity>
        where TUnitOfWork : IUnitOfWork
    {
        protected IValidationDictionary validationDictionary;
        protected abstract TUnitOfWork UnitOfWork { get; set; }
        protected abstract TRepository Repository { get; set; }

        public BaseService(IValidationDictionary validationDictionary)
        {
            this.validationDictionary = validationDictionary;
        }
        
        protected abstract bool PreValidate(TEntity itemToValidate);

        public IQueryable<TEntity> GetAll(Func<TEntity, bool> filter = null)
        {
            return Repository.GetAll(filter);
        }

        public TEntity GetById(int id)
        {
            return Repository.GetById(id);
        }

        public bool DeleteById(int id)
        {
            Repository.DeleteById(id);
            return UnitOfWork.Save() > 0;
        }

        public bool AddOrUpdate(TEntity item)
        {
            // Validation Logic
            if (!PreValidate(item))
                return false;

            // Database Logic
            try
            {
                Repository.AddOrUpdate(item);
            }
            catch
            {
                return false;
            }

            return UnitOfWork.Save() > 0;
        }
    }
}
