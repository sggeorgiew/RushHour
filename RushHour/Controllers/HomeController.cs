﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RushHour.Data;
using RushHour.Helpers;
using RushHour.Data.Entities;

namespace RushHour.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult Admin()
        {
            return View();
        }
    }
}