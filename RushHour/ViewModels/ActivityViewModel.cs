﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RushHour.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace RushHour.ViewModels
{
    public class ActivityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Duration (in minutes)")]
        public float Duration { get; set; }

        public decimal Price { get; set; }

        #region Constructors
        public ActivityViewModel()
        { }

        public ActivityViewModel(Activity activity)
        {
            this.Id = activity.Id;
            this.Name = activity.Name;
            this.Duration = activity.Duration;
            this.Price = activity.Price;
        }
        #endregion
    }
}