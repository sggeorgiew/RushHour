﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using RushHour.Data.Entities;

namespace RushHour.ViewModels
{
    public class AppointmentDetailsViewModel : AppointmentViewModel
    {
        [Display(Name = "Activities")]
        public List<Activity> ChosenActivities { get; set; }

        public AppointmentDetailsViewModel(Appointment appointment) : base(appointment)
        {
            this.ChosenActivities = appointment.Activities.ToList();
        }
    }
}