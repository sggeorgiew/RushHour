﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RushHour.Data.Entities;

namespace RushHour.ViewModels
{
    public class AppointmentPagingViewModel : BasePagingViewModel<AppointmentViewModel>
    {
        public AppointmentPagingViewModel(List<Appointment> list, int pageIndex, int recordsCount) : base()
        {
            list.ForEach(item => Items.Add(new AppointmentViewModel(item)));

            TotalItems = recordsCount;
            TotalPages = ((recordsCount - 1) / ItemsPerPage) + 1;
            CurrentPageIndex = pageIndex;
        }
    }
}