﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RushHour.Data.Entities;

namespace RushHour.Data.Repositories
{
    public interface IBaseRepository<T> : IDisposable where T : BaseEntity
    {
        T GetById(int id);

        IQueryable<T> GetAll(Func<T, bool> filter = null);

        void AddOrUpdate(T item);

        bool DeleteById(int id);
    }
}
