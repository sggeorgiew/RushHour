﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private RushHourContext Context = new RushHourContext();

        private UserRepository userRepository;
        private ActivityRepository activityRepository;
        private AppointmentRepository appointmentRepository;

        public UserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new UserRepository(Context);
                }

                return this.userRepository;
            }
        }

        public ActivityRepository ActivityRepository
        {
            get
            {
                if (this.activityRepository == null)
                {
                    this.activityRepository = new ActivityRepository(Context);
                }

                return this.activityRepository;
            }
        }

        public AppointmentRepository AppointmentRepository
        {
            get
            {
                if (this.appointmentRepository == null)
                {
                    this.appointmentRepository = new AppointmentRepository(Context);
                }

                return this.appointmentRepository;
            }
        }

        public int Save()
        {
            return Context.SaveChanges();
        }
    }
}
