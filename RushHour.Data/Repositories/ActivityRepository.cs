﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RushHour.Data.Entities;

namespace RushHour.Data.Repositories
{
    public class ActivityRepository : BaseRepository<Activity>
    {
        public ActivityRepository(RushHourContext context) : base(context)
        { }
    }
}
